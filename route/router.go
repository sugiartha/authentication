package route

import (
	"authentication/api"
	"authentication/config"
	"net/http"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func Init() *echo.Echo {
	configuration := config.GetConfig()
	e := echo.New()

	// CORS restricted
	// Allows requests from any listed url origin
	// wth GET, PUT, POST or DELETE method.
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{configuration.CORS},
		AllowMethods: []string{http.MethodGet, http.MethodPut, http.MethodPost, http.MethodDelete},
	}))

	e.GET("/", api.Home)

	e.GET("/users", api.GetUsers)

	e.POST("/login", api.PostLogin)

	return e
}
